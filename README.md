[![Pipeline Status](https://gitlab.com/rascul/pasteb/badges/master/pipeline.svg)](https://gitlab.com/rascul/pasteb/pipelines)
[![Coverage](https://gitlab.com/rascul/pasteb/badges/master/coverage.svg?job=cov)](https://rascul.gitlab.io/pasteb/cov/index.html)
[![MIT License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/rascul/pasteb/blob/master/LICENSE)

pastebucket
===========

A pastebin like web application. Useful for pasting text into a form to store.
Syntax highlighting is selected via a dropdown menu and is applied server side.
No javascript to load, just some CSS.

Curl may be used to submit text, but there is no option at this time to specify
the language (for syntax highlighting) via curl:
```bash
cargo check | curl -T- https://p.rascul.xyz
curl -T- https://p.rascul.xyz < src/main.rs
```

The git repo is located at https://gitlab.com/rascul/pasteb.
There is an instance running at https://p.rascul.xyz.

pastebucket is written in [rust](https://rust-lang.org) and uses the
[gotham](https://gotham.rs) web application framework.

## Building

First, clone the repo:
```bash
git clone https://gitlab.com/rascul/pasteb
```

Enter the directory and create your configuration:
```bash
cd pasteb
cp config.toml.default config.toml
$EDITOR config.toml
```

Build pastebucket
```bash
cargo build --release
```

## Running

Run pastebucket
```bash
cargo run --release
```

It is best to run a web server such as [Apache](https://httpd.apache.org) or
[nginx](https://nginx.org) and reverse proxy connections to pastebucket.

You may run the executable directly (without cargo), it is located at
`target/release/pastebucket`.

pastebucket should be run from the directory containing `config.toml`. At this
time there is no option specify where to load the config from via the command
line.

A sample systemd service file is included in the git repo.
