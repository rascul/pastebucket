use askama::Template;

use gotham::helpers::http::response::create_response;
use gotham::state::State;

use hyper::{Body, Response, StatusCode};
use log::error;
use mime;

#[derive(Debug, Template)]
#[template(path = "error.html")]
pub struct HtmlError {
	site_url: String,
	error_number: u16,
	error_description: String,
}

pub fn create_html_error_response(
	status: StatusCode,
	site_url: String,
	state: &State,
) -> Response<Body> {
	let template = HtmlError {
		site_url,
		error_number: status.as_u16(),
		error_description: status.canonical_reason().unwrap_or("").into(),
	};

	match template.render() {
		Ok(content) => create_response(&state, status, mime::TEXT_HTML_UTF_8, content.into_bytes()),
		Err(e) => {
			error!("error_response.rs(10): {:?}", e);
			create_response(
				&state,
				StatusCode::INTERNAL_SERVER_ERROR,
				mime::TEXT_PLAIN,
				Body::from("500 INTERNAL SERVER ERROR"),
			)
		}
	}
}

pub fn create_text_error_response(status: StatusCode, state: &State) -> Response<Body> {
	create_response(&state, status, mime::TEXT_PLAIN, status.canonical_reason().unwrap_or_default())
}
