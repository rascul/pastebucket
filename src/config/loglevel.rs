use std::default::Default;

use log::LevelFilter;
use serde_derive::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub enum LogLevel {
	#[allow(non_camel_case_types)]
	error,
	#[allow(non_camel_case_types)]
	warn,
	#[allow(non_camel_case_types)]
	info,
	#[allow(non_camel_case_types)]
	debug,
	#[allow(non_camel_case_types)]
	trace,
}

impl Default for LogLevel {
	fn default() -> LogLevel {
		LogLevel::info
	}
}

impl LogLevel {
	pub fn level(&self) -> LevelFilter {
		match &self {
			LogLevel::error => LevelFilter::Error,
			LogLevel::warn => LevelFilter::Warn,
			LogLevel::info => LevelFilter::Info,
			LogLevel::debug => LevelFilter::Debug,
			LogLevel::trace => LevelFilter::Trace,
		}
	}
}
