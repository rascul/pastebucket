mod edit;
mod index;
mod raw;
mod submit;
pub mod view;

use gotham::handler::assets::FileOptions;
use gotham::middleware::logger::RequestLogger;
use gotham::middleware::state::StateMiddleware;
use gotham::pipeline::new_pipeline;
use gotham::pipeline::single::single_pipeline;
use gotham::router::builder::*;
use gotham::router::Router;

use crate::config::Config;

pub fn build(config: Config) -> Router {
	let (chain, pipeline) = single_pipeline(
		new_pipeline()
			.add(RequestLogger::new(log::Level::Info))
			.add(StateMiddleware::new(config.clone()))
			.build(),
	);

	build_router(chain, pipeline, |route| {
		route.get("/").to(index::route);
		route.post("/").to(submit::post);
		route.put("/").to(submit::put);
		route
			.get("/s/*")
			.to_dir(FileOptions::new(config.static_directory));
		route
			.get("/:id")
			.with_path_extractor::<view::Params>()
			.to(view::get);
		route
			.get("/:id/edit")
			.with_path_extractor::<edit::Params>()
			.to(edit::route);
		route
			.get("/:id/raw")
			.with_path_extractor::<raw::Params>()
			.to(raw::get);
	})
}
