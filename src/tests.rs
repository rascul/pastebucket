use gotham::test::TestServer;
use hyper::StatusCode;
use lazy_static::lazy_static;
use tempfile::{tempdir, TempDir};

use crate::config::Config;
use crate::routes;

lazy_static! {
	static ref TEMPDIR: TempDir = tempdir().unwrap();
	static ref CONFIG: Config = {
		let mut c: Config = Default::default();
		c.data_directory = TEMPDIR.path().into();
		c
	};
	static ref TEST_SERVER: TestServer = TestServer::new(routes::build(CONFIG.clone())).unwrap();
}

#[test]
fn get_index() {
	let res = TEST_SERVER.client().get(&CONFIG.url).perform().unwrap();
	assert_eq!(res.status(), StatusCode::OK);
}

#[test]
fn submit_html_and_check_html() {
	use askama::Template;
	use syntect::html::ClassedHTMLGenerator;

	use crate::routes::view::View;
	use crate::paste::Paste;
	use crate::syntax::SYNTAX_SET;

	let res = TEST_SERVER
		.client()
		.post(
			&CONFIG.url,
			"paste=test",
			mime::APPLICATION_WWW_FORM_URLENCODED,
		)
		.perform()
		.unwrap();
	assert_eq!(res.status(), StatusCode::SEE_OTHER);

	let location: String = res.headers().get("Location").unwrap().to_str().unwrap().into();
	let id: String = location.clone().split('/').last().unwrap().into();
	let mut new_url = CONFIG.url.clone();
	new_url.push_str(&location);

	let res = TEST_SERVER.client().get(new_url).perform().unwrap();
	assert_eq!(res.status(), StatusCode::OK);
	let body = res.read_utf8_body().unwrap();

	let mut path = CONFIG.data_directory.clone();
	path.push(&id);
	let paste = Paste::from_file(path).unwrap();

	let text_lines: Vec<String> = paste.text.lines().map(|s| s.into()).collect();
	let index_len = format!("{}", text_lines.len()).len();
	let syntax = SYNTAX_SET
		.find_syntax_by_name(&paste.lang)
		.unwrap_or_else(|| SYNTAX_SET.find_syntax_plain_text());
	let mut high_lines: Vec<String> = Vec::new();

	for line in text_lines {
		let mut html_generator = ClassedHTMLGenerator::new(&syntax, &SYNTAX_SET);
		html_generator.parse_html_for_line(&line);
		high_lines.push(html_generator.finalize());
	}

	let template: View = View {
		id,
		dt: paste.dt.format("%Y-%m-%dT%H:%MZ").to_string(),
		text_lines: high_lines,
		index_len,
		site_url: CONFIG.url.clone(),
		syntax: paste.lang,
	};

	let content = template.render().unwrap();
	assert_eq!(body, content);
}

#[test]
fn submit_text_and_check_raw() {
	let res = TEST_SERVER
		.client()
		.put(&CONFIG.url, "test", mime::TEXT_PLAIN)
		.perform()
		.unwrap();
	assert_eq!(res.status(), StatusCode::OK);

	let redirect_url = res.read_utf8_body().unwrap();
	let new_url = format!("{}/raw", redirect_url.trim_end());
	let res = TEST_SERVER.client().get(new_url).perform().unwrap();
	let body = res.read_utf8_body().unwrap();
	assert_eq!(body, "test");
}
