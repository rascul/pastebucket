pub mod loglevel;

use std::default::Default;
use std::fs::File;
use std::io::Read;
use std::path::{Path, PathBuf};

use gotham_derive::StateData;
use serde_derive::Deserialize;
use toml;

pub use crate::config::loglevel::LogLevel;
use crate::result::Result;

#[derive(Clone, Debug, Deserialize, StateData)]
pub struct Config {
	#[serde(default)]
	pub address: String,
	#[serde(default)]
	pub url: String,
	#[serde(default)]
	pub log_level: LogLevel,
	#[serde(default)]
	pub log_file: Option<String>,
	#[serde(default)]
	pub data_directory: PathBuf,
	#[serde(default)]
	pub template_directory: PathBuf,
	#[serde(default)]
	pub static_directory: PathBuf,
	#[serde(default)]
	pub salt: String,
}

impl Default for Config {
	fn default() -> Self {
		Self {
			address: "127.0.0.1:9214".into(),
			url: "http://127.0.0.1:9214".into(),
			log_level: LogLevel::info,
			log_file: None,
			data_directory: "data".into(),
			template_directory: "templates".into(),
			static_directory: "static".into(),
			salt: "salt and pepper".into(),
		}
	}
}

impl Config {
	pub fn load<T: AsRef<Path>>(path: T) -> Result<Config> {
		if let Ok(mut file) = File::open(path.as_ref()) {
			let mut buf = String::new();
			file.read_to_string(&mut buf)?;
			let config: Config = toml::from_str(&buf)?;
			Ok(config)
		} else {
			Ok(Config::default())
		}
	}
}
