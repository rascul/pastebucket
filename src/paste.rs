use std::collections::HashMap;
use std::fs::File;
use std::io::{BufReader, BufWriter, Error, ErrorKind, Read, Write};
use std::path::PathBuf;

use chrono::{DateTime, Utc};
use harsh::HarshBuilder;
use serde::{Deserialize, Serialize};
use toml;

use crate::result::Result;

#[derive(Debug, Deserialize, Serialize)]
pub struct Paste {
	pub id: String,
	pub dt: DateTime<Utc>,
	pub lang: String,
	pub text: String,
}

impl Paste {
	pub fn from_file(path: PathBuf) -> Result<Self> {
		let file = File::open(path)?;
		let mut reader = BufReader::new(file);
		let mut buf = String::new();
		reader.read_to_string(&mut buf)?;

		let paste: Paste = toml::from_str(&buf)?;

		Ok(paste)
	}

	pub fn from_text(text: String, salt: String) -> Result<Self> {
		let dt = Utc::now();
		let harsh = HarshBuilder::new().salt(salt).init()?;
		let encoded = harsh.encode(&[dt.timestamp_millis() as u64]);

		if let Some(id) = encoded {
			Ok(Paste {
				id,
				dt,
				text,
				lang: "Plain Text".into(),
			})
		} else {
			Err(Box::new(Error::new(
				ErrorKind::Other,
				"couldn't generate hash",
			)))
		}
	}

	pub fn from_form(form: HashMap<String, String>, salt: String) -> Result<Self> {
		let dt = Utc::now();
		let harsh = HarshBuilder::new().salt(salt).init()?;
		let encoded = harsh.encode(&[dt.timestamp_millis() as u64]);

		if let Some(id) = encoded {
			let text: String = if let Some(t) = form.get("paste") {
				t.into()
			} else {
				"".into()
			};

			let lang: String = if let Some(l) = form.get("lang") {
				l.into()
			} else {
				"Plain Text".into()
			};

			Ok(Paste { id, dt, lang, text })
		} else {
			Err(Box::new(Error::new(ErrorKind::Other, "empty hash")))
		}
	}

	pub fn to_file(&self, path: PathBuf) -> Result<()> {
		let file = File::create(path)?;
		let mut writer = BufWriter::new(file);
		writer.write_all(toml::to_string_pretty(&self)?.as_bytes())?;
		Ok(())
	}
}
