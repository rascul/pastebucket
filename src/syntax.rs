use lazy_static::lazy_static;
use syntect::parsing::SyntaxSet;

lazy_static! {
	pub static ref SYNTAX_SET: SyntaxSet = SyntaxSet::load_defaults_newlines();
	pub static ref SYNTAXES: Vec<String> = {
		let syntax_set = SyntaxSet::load_defaults_newlines();
		let mut syntaxes: Vec<String> = Vec::new();

		for syntax in syntax_set.syntaxes() {
			if !syntax.hidden {
				syntaxes.push(syntax.name.clone());
			}
		}

		syntaxes.sort();
		syntaxes
	};
}
