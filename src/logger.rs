use chrono::Local;
use colored::Colorize;
use fern::colors::{Color, ColoredLevelConfig};
use fern::Dispatch;

use crate::config::LogLevel;
use crate::result::Result;

pub fn setup(level: &LogLevel) -> Result<()> {
	Dispatch::new()
		.format(|out, message, record| {
			let colors = ColoredLevelConfig::new()
				.error(Color::Red)
				.warn(Color::Magenta)
				.info(Color::Cyan)
				.debug(Color::Yellow)
				.trace(Color::Green);

			out.finish(format_args!(
				"{} {} {}",
				Local::now()
					.format("%Y-%m-%dT%H:%M:%S%.3f%z")
					.to_string()
					.white()
					.bold(),
				colors.color(record.level()),
				message
			))
		})
		.level(level.level())
		.chain(std::io::stdout())
		.apply()?;

	Ok(())
}
