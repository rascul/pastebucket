use askama::Template;
use futures::future;

use gotham::handler::HandlerFuture;
use gotham::helpers::http::response::create_response;
use gotham::state::{FromState, State};
use gotham_derive::{StateData, StaticResponseExtender};

use hyper::StatusCode;
use log::error;
use mime;
use serde_derive::Deserialize;

use crate::config::Config;
use crate::error_response::create_html_error_response;
use crate::paste::Paste;
use crate::syntax::SYNTAXES;

#[derive(Deserialize, StateData, StaticResponseExtender)]
pub struct Params {
	id: String,
}

#[derive(Debug, Template)]
#[template(path = "edit.html")]
pub struct Edit {
	id: String,
	dt: String,
	text: String,
	site_url: String,
	syntaxes: Vec<String>,
	syntax: String,
}

pub fn route(mut state: State) -> Box<HandlerFuture> {
	Box::new({
		let config = Config::take_from(&mut state);
		let Params { id } = Params::take_from(&mut state);

		let mut path = config.data_directory.clone();
		path.push(id.clone());

		let res = match Paste::from_file(path) {
			Ok(paste) => {
				let template = Edit {
					id,
					dt: paste.dt.format("%Y-%m-%dT%H:%MZ").to_string(),
					text: paste.text,
					site_url: config.url.clone(),
					syntaxes: SYNTAXES.to_vec(),
					syntax: paste.lang,
				};

				match template.render() {
					Ok(content) => create_response(
						&state,
						StatusCode::OK,
						mime::TEXT_HTML_UTF_8,
						content.into_bytes(),
					),
					Err(e) => {
						error!("edit.rs(10): {:?}", e);
						create_html_error_response(StatusCode::INTERNAL_SERVER_ERROR, config.url, &state)
					}
				}
			}
			Err(e) => {
				error!("edit.rs(20): {:?}", e);
				create_html_error_response(StatusCode::INTERNAL_SERVER_ERROR, config.url, &state)
			}
		};

		future::ok((state, res))
	})
}
