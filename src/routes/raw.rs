use futures::future;

use gotham::handler::HandlerFuture;
use gotham::helpers::http::response::create_response;
use gotham::state::{FromState, State};
use gotham_derive::{StateData, StaticResponseExtender};

use hyper::StatusCode;
use log::error;
use mime;
use serde_derive::Deserialize;

use crate::config::Config;
use crate::error_response::create_text_error_response;
use crate::paste::Paste;

#[derive(Deserialize, StateData, StaticResponseExtender)]
pub struct Params {
	id: String,
}

pub fn get(mut state: State) -> Box<HandlerFuture> {
	Box::new({
		let config = Config::take_from(&mut state);
		let Params { id } = Params::take_from(&mut state);

		let mut path = config.data_directory;
		path.push(id);

		let res = match Paste::from_file(path) {
			Ok(paste) => create_response(&state, StatusCode::OK, mime::TEXT_PLAIN, paste.text),
			Err(e) => {
				error!("raw.rs: {:?}", e);
				create_text_error_response(StatusCode::INTERNAL_SERVER_ERROR, &state)
			}
		};

		future::ok((state, res))
	})
}
